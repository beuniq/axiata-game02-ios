//
//  MoveAbleSprite.swift
//  Test
//
//  Created by Beuniq on 26/05/2017.
//  Copyright © 2017 Beuniq. All rights reserved.
//

import Foundation
import SpriteKit

enum spriteType : Int
{
    case goodSprite,
    badSprite
}

enum spriteName : Int
{
    case good_1 = 0,
    good_2,
    good_3,
    good_4,
    good_5,
    good_6,
    good_7,
    good_8,
    good_9,
    good_10,
    
    bad_1,
    bad_2,
    bad_3,
    bad_4,
    bad_5,
    bad_6,
    bad_7,
    bad_8,
    bad_9,
    bad_10
}

func random() -> CGFloat {
    return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
}

func random(min: CGFloat, max: CGFloat) -> CGFloat
{
    return random() * (max-min) + min
}


class MoveableSprite : SKSpriteNode {
    
    let spriteType : spriteType
    var spriteTexture : SKTexture
    var randomNum = 0
    var currentNameSprite = ""
    var spriteSpeed : CGFloat = 0
    var time : CGFloat = 0
    var isHold = false
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("NSCoding not supported")
    }
    
    
    init(typeSprite: spriteType, number : Int, checkingBool : Bool)
    {
        self.spriteType = typeSprite
//        self.randomNum = number
        
        if(checkingBool == false)
        {
            if(typeSprite == .goodSprite)
            {
                //            randomNum = Int(arc4random_uniform(4))
                randomNum = Int(arc4random_uniform(UInt32(number)))
            }
            else
            {
                //            randomNum = Int(arc4random_uniform(4) + 4)
                randomNum = Int(arc4random_uniform(UInt32(number)) + 10)
            }

        }
        
        else
        {
            randomNum = number
        }
        
        if let findingSprite = spriteName(rawValue: randomNum)
        {
            switch findingSprite
            {
                case .good_1 :
                    currentNameSprite = "accept_01"
                case .good_2 :
                    currentNameSprite = "accept_02"
                case .good_3 :
                    currentNameSprite = "accept_03"
                case .good_4 :
                    currentNameSprite = "accept_04"
                case .good_5 :
                    currentNameSprite = "accept_05"
                case .good_6 :
                    currentNameSprite = "accept_06"
                case .good_7 :
                    currentNameSprite = "accept_07"
                case .good_8 :
                    currentNameSprite = "accept_08"
                case .good_9 :
                    currentNameSprite = "accept_09"
                case .good_10 :
                    currentNameSprite = "accept_10"
                
                case .bad_1 :
                    currentNameSprite = "decline_01"
                case .bad_2 :
                    currentNameSprite = "decline_02"
                case .bad_3 :
                    currentNameSprite = "decline_03"
                case .bad_4 :
                    currentNameSprite = "decline_04"
            case .bad_5 :
                currentNameSprite = "decline_05"
            case .bad_6 :
                currentNameSprite = "decline_06"
            case .bad_7 :
                currentNameSprite = "decline_07"
            case .bad_8 :
                currentNameSprite = "decline_08"
            case .bad_9 :
                currentNameSprite = "decline_09"
            case .bad_10 :
                currentNameSprite = "decline_10"
            }
        }
        
       
        spriteTexture = SKTexture(imageNamed: currentNameSprite)
        
        super.init(texture: spriteTexture, color: .clear , size: spriteTexture.size())
//        randomNum = self.getRandomNum()
    }
    
    func changeSpriteWhenTouch()
    {
        if(self.isHold == false)
        {
            self.isHold = true
            self.texture = SKTexture(imageNamed: "play")
        }
        else
        {
            self.isHold = false
            self.texture = SKTexture(imageNamed: checkingWhichSprite())
        }
    }
    
    func checkingWhichSprite() -> String
    {
        var tempString = ""
        
        if let findingSprite = spriteName(rawValue: self.randomNum)
        {
            switch findingSprite
            {
            case .good_1 :
               tempString = "accept_01"
            case .good_2 :
                tempString = "accept_02"
            case .good_3 :
                tempString = "accept_03"
            case .good_4 :
                tempString = "accept_04"
            case .good_5 :
                tempString = "accept_05"
            case .good_6 :
                tempString = "accept_06"
            case .good_7 :
                tempString = "accept_07"
            case .good_8 :
                tempString = "accept_08"
            case .good_9 :
                tempString = "accept_09"
            case .good_10 :
                tempString = "accept_10"
            
            case .bad_1 :
                tempString = "decline_01"
            case .bad_2 :
                tempString = "decline_02"
            case .bad_3 :
                tempString = "decline_03"
            case .bad_4 :
                tempString = "decline_04"
            case .bad_5 :
                tempString = "decline_05"
            case .bad_6 :
                tempString = "decline_06"
            case .bad_7 :
                tempString = "decline_07"
            case .bad_8 :
                tempString = "decline_08"
            case .bad_9 :
                tempString = "decline_09"
            case .bad_10 :
                tempString = "decline_10"
            }
        }
        return tempString
    }

    
    func getRandomNum() -> Int
    {
        return self.randomNum
    }
    
    func checkingNew(number : Int) -> Bool
    {
        if(self.spriteType == .goodSprite)
        {
            if(randomNum == number - 1)
            {
                return true
            }
        }
        else if(self.spriteType == .badSprite)
        {
            if(randomNum == (number - 1) + 4)
            {
                return true
            }
        }
        
        return false
    }
    
    func getSpriteType() -> spriteType
    {
        return self.spriteType
    }
    
    func setSpeed(saveSpeed : CGFloat)
    {
        self.spriteSpeed = saveSpeed
    }
    
    func getSpeed() -> CGFloat
    {
        return self.spriteSpeed
    }
    
    func setTime(saveTime: CGFloat)
    {
        self.time = saveTime
    }
    
    func getTime() -> CGFloat
    {
        return self.time
    }
       
}
