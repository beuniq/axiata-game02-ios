//
//  GameScene.swift
//  Test
//
//  Created by Beuniq on 22/05/2017.
//  Copyright © 2017 Beuniq. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

//some extension for SKLabelNode
extension SKLabelNode {
    //to make sentence have a line
    func multilined(lineSpacing : CGFloat) -> SKLabelNode {
        let substrings: [String] = self.text!.components(separatedBy: "\n")
        var y : CGFloat = 0
        return substrings.enumerated().reduce(SKLabelNode()) {
            let label = SKLabelNode(fontNamed: self.fontName)
            label.text = $1.element
            label.fontColor = self.fontColor
            label.fontSize = self.fontSize
            label.position = self.position
            label.horizontalAlignmentMode = self.horizontalAlignmentMode
            label.verticalAlignmentMode = self.verticalAlignmentMode
            //if not the first then move the other text below the origin one
            if($1.offset != 0)
            {
                y += (CGFloat(lineSpacing) + CGFloat(self.fontSize))
            }
            label.position = CGPoint(x: 0, y: -y)
            $0.addChild(label)
            return $0
        }
    }
    
    //shadow for the text
    convenience init?(fontNamed font: String, andText text: String, andSize size: CGFloat, withShadow shadow: UIColor) {
        self.init(fontNamed: font)
        self.text = text
        self.fontSize = size
        
        let shadowNode = SKLabelNode(fontNamed: font)
        shadowNode.text = self.text
        shadowNode.zPosition = self.zPosition - 1
        shadowNode.fontColor = shadow
        // Just create a little offset from the main text label
        shadowNode.position = CGPoint(x: 4, y: -4)
        shadowNode.fontSize = self.fontSize
        shadowNode.alpha = 0.5
        
        self.addChild(shadowNode)
    }
}

//calculate the throw object
func gradient (firstPosition : CGPoint , lastPosition : CGPoint , screenHeight : CGFloat, screenWidth : CGFloat) -> CGPoint
{
    var tempPoint : CGPoint = CGPoint(x: 0, y: 0)
    
    if firstPosition.x == lastPosition.x && firstPosition.y < lastPosition.y {
        // shoot up
        tempPoint = CGPoint(x: lastPosition.x , y: screenHeight + 40)
    }
    else if firstPosition.x == lastPosition.x && firstPosition.y > lastPosition.y {
        // shoot down
        tempPoint = CGPoint(x: lastPosition.x , y: -40)
    }
    else if firstPosition.x < lastPosition.x {
        // shoot right
        // y = mx + c
        // to get m
        let m = (lastPosition.y - firstPosition.y) / (lastPosition.x - firstPosition.x)
        
        // to get c
        let c = lastPosition.y - (m * lastPosition.x)
        
        // finf new point
        let newPoint = (m * (screenWidth + 40)) + c
        tempPoint = CGPoint(x: screenWidth + 40 , y: newPoint)
        
    }
    else if firstPosition.x > lastPosition.x {
        // shoot left
        // y = mx + c
        // to get m
        let m = (lastPosition.y - firstPosition.y) / (lastPosition.x - firstPosition.x)
        
        // to get c
        let c = lastPosition.y - (m * lastPosition.x)
        
        // finf new point
        let newPoint = (m * -40) + c
        tempPoint = CGPoint(x: -40 , y: newPoint)
        
    }
    
    return tempPoint
}

//layering of the collision detection
struct PhysicsCategory{
    static let None : UInt32 = 0
    static let All : UInt32 = UInt32.max
    static let playerPhysic : UInt32 = 1
    static let goodStuffPhysic : UInt32 = 2
    static let badSTuffPhysic : UInt32 = 3
}

enum gameState
{
    case inGame
    case isTutorialShow_1
    case intro
    case pause
    case gameOver
}

class GameTwo: SKScene , SKPhysicsContactDelegate {
    
    let player = SKSpriteNode(imageNamed: "guy_01") // the guy in the middle
    
    //for the guy/character animation array
    var textureAtlas_1 = SKTextureAtlas()
    var textureArray_1 = [SKTexture]()
    
    //character animation timer
    var characAnimTimer = Timer()
    
    var fingerShow = SKSpriteNode(imageNamed: "finger_down")
    var textureAtlas_finger = SKTextureAtlas()
    var textureArray_finger = [SKTexture]()
    
    //how many per second the bad/good stuff will be spawn for every second
    var durationSpawnGoodThings = 1.0
    var durationSpawnBadThings = 1.0
    //the spawn good/bad stuff timer
    var spawnGoodSprite = Timer()
    var spawnBadSprite = Timer()
    
    //for the indicator show good/badd stuff
    var goodTextDisplay = ["Bottle","Note","Pendrive", "Apple", "Recycle Bag" , "Movie Ticket", "Pencil", "Keychain", "Coffee", "Ball" ]
    var badTextDisplay = ["Phone","Cash","Watch", "Camera", "Wallet" , "Diamond", "Loudspeaker", "Holiday Ticket", "Computer", "Hamper"]
    var arrayNumGood = [Int]()
    var arrayNumBad = [Int]()
    var currentPoolGoodArray = [Int]()
    var currentPoolBadArray = [Int]()
    
    //the indicator to show good/bad
    var showNewBadSprite = false
    var showNewGoodSprite = false
    var changeBadGood = false
    
    //counting how many bad and good stuff
    var countGoodStuff = 0
    var countBadStuff = 0
    var currentNumberGoodSprite = 0
    var currentNumberBadSprite = 0
    
    var currentchild = SKSpriteNode() //for the one we currently dragging the bubble
    
    //for calculation stuff
    var firstTouchObj = CGPoint() //where is first touch the bubble (moveableSprite)
    var lastTouchObj = CGPoint() ////where is last touch the bubble (moveableSprite)
    
    //this part is mainly checking for the touch move for the sprite
    var checkTrue = false
    var moveableMove = false
    var checkingUpdate = true
    
    //setting up the time to 0 n intro time is 3
    var secTime = 0
    var gameScore = 0
    var highscore = 0
    var introTime = 3
    
    //timer stuff
    var timer = Timer() //for the game timer on top middle
    var introTimer = Timer() //for intro timer
    
    //label for intro n top timer
    var introLabelTimer = SKLabelNode()
    var scoreLabel = SKLabelNode()
    
    var currentGameState = gameState.intro
    var stopOnceDuringExplaination = false
    
    //make another node in the game for modify certain group stuff
    var moveableParents = SKNode() //for the good/bad stuff buble all inside here
    var UIdisplayNode = SKNode() //the showing bad/good indicator coming
    var UIGameOverNode = SKNode() //for game over one
    var introNode = SKNode() // for the introduction one
    var showBadFirstTimeNode = SKNode()
    var showFirstTimeTutorial = SKNode()
    
    var destroyUIDisplay = Timer()
    var endGameButton = SKSpriteNode(imageNamed: "btn_continue_01")
    var playAbleWidth : CGFloat = 0
    
    //var temp stuff for
    var isTutorialisShow_1 = 0
    var bgTutorial = SKSpriteNode()
    var handPointing = SKSpriteNode()
    
    let userDefults = UserDefaults.standard //returns shared defaults object.
    var bgSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "Arcade-Puzzler", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()
    
    override func didMove(to view: SKView) {
        
        introBeforeStart()
        //        self.view?.showsPhysics = true // to show physic collider of the object
        //        self.view?.isUserInteractionEnabled = true
        //        self.view?.isMultipleTouchEnabled = false //making multiple touch disable
    }
    
    func introBeforeStart()
    {
        physicsWorld.gravity = CGVector.zero //zero gravity for the physic object
        physicsWorld.contactDelegate = self //make the physic stuff happen in self(our device)
        
        //getting the aspect ratio of the ui of the apps using
        let screenSize2 = UIScreen.main.bounds
        let maxAspectRatio: CGFloat = screenSize2.height / screenSize2.width
        playAbleWidth = size.height / maxAspectRatio
        
        //making the array have 0-9(10 stuff) in the corresponding array
        for index in 0..<10{
            arrayNumGood.append(index)
            arrayNumBad.append(index)
        }
        
        //random the position of the corresping array & then take out the number from the corresponding index and put inside the current pool arrray
        //and also remove the number choosen from prev array
        let tempGood = Int(arc4random_uniform(UInt32(arrayNumGood.count)))
        currentNumberGoodSprite = arrayNumGood[tempGood]
        arrayNumGood = arrayNumGood.filter(){$0 != currentNumberGoodSprite}
        currentPoolGoodArray.append(currentNumberGoodSprite)
        
        let tempBad = Int(arc4random_uniform(UInt32(arrayNumBad.count)))
        currentNumberBadSprite = arrayNumBad[tempBad]
        arrayNumBad = arrayNumBad.filter(){$0 != currentNumberBadSprite}
        currentPoolBadArray.append(currentNumberBadSprite)
        
        //setting up the bg stuff for the game
        backgroundColor = SKColor.white
        let bg = SKSpriteNode(imageNamed: "bg")
        bg.size = self.size
        bg.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        bg.zPosition = -1
        addChild(bg)
        
        //setting up the player position
        player.position = CGPoint(x: size.width/2 , y: (size.height/2) - 200 )
        player.zPosition = 10
        
        //setting up the player collider in the middle of character n in rectagle shape
        let path2 = CGMutablePath()
        path2.addLines(between: [CGPoint(x: -120, y: 50),CGPoint(x: 100, y: 50), CGPoint(x: 100, y: -150), CGPoint(x: -120, y: -150),CGPoint(x: -120, y: 70)
            ])
        path2.closeSubpath()
        player.physicsBody = SKPhysicsBody(polygonFrom: path2)
        player.physicsBody!.isDynamic = true
        player.physicsBody!.categoryBitMask = PhysicsCategory.playerPhysic
        player.physicsBody!.contactTestBitMask =  PhysicsCategory.badSTuffPhysic
        player.physicsBody!.collisionBitMask = PhysicsCategory.None
        addChild(player)
        
        //setting up the rest of the constant sprite
        let windowLeft = SKSpriteNode(imageNamed: "window")
        windowLeft.position = CGPoint(x: (self.size.width/2 - (400 + windowLeft.size.width/2) ), y: self.size.height/2 + 100)
        addChild(windowLeft)
        
        let windowRight = SKSpriteNode(imageNamed: "window")
        windowRight.zPosition = 1
        windowRight.position = CGPoint(x:(self.size.width/2 + (400 + windowLeft.size.width/2)) , y: self.size.height/2 + 100) //self.size.width + 10
        addChild(windowRight)
        
        let mineralWater = SKSpriteNode(imageNamed: "water_machine")
        mineralWater.zPosition = 2
        mineralWater.position = CGPoint(x: player.position.x + 300 , y: player.position.y)
        addChild(mineralWater)
        
        let axiataPic = SKSpriteNode(imageNamed: "logo_frame")
        axiataPic.position = CGPoint(x: player.position.x - 200, y: player.position.y + 300)
        addChild(axiataPic)
        
        let clockTimer = SKSpriteNode(imageNamed: "clock")
        
        scoreLabel = SKLabelNode(fontNamed: "AllerDisplay")
        scoreLabel.fontSize = 80
        scoreLabel.zPosition = 13
        scoreLabel.fontColor = UIColor(red: 88/255.0, green: 136/255.0, blue: 184/255.0, alpha: 1.0)
        scoreLabel.position = CGPoint(x: size.width/2 + (clockTimer.size.width/2), y: size.height - 125 )
        scoreLabel.text = "00:00"
        addChild(scoreLabel)
        
        clockTimer.zPosition = 13
        clockTimer.position = CGPoint(x: scoreLabel.position.x - 170 , y: size.height - 100)
        addChild(clockTimer)
        
        addChild(introNode)
        
        let rect1 = SKShapeNode(rect: CGRect(x:self.size.width/2 - ((playAbleWidth-100) / 2) , y: 50 , width: playAbleWidth - 100, height: 550),cornerRadius: 20)
        rect1.fillColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
        rect1.strokeColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
        rect1.zPosition = 98
        introNode.addChild(rect1)
        
        let rect2 = SKShapeNode(rect: CGRect(x:self.size.width/2 - ((playAbleWidth-150)/2) , y: 80, width: playAbleWidth - 150, height: 500),cornerRadius: 20)
        rect2.fillColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        rect2.strokeColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        rect2.zPosition = 99
        introNode.addChild(rect2)
        
        var showSpriteDisplayGood = SKSpriteNode()
        var showSpriteDisplayBad = SKSpriteNode()
        let textGoodShowDisplay = SKLabelNode(fontNamed: "H.H. Samuel")
        let textBadShowDisplay = SKLabelNode(fontNamed: "H.H. Samuel")
        var iconGoodDisplay = SKSpriteNode()
        var iconBadDisplay = SKSpriteNode()
        
        //the current good sprite
        showSpriteDisplayGood = SKSpriteNode(texture: getShowSprite(typeSpriteUi: "good", currentNumber: (currentNumberGoodSprite + 1)))
        showSpriteDisplayGood.position = CGPoint(x: self.size.width/2, y: (rect2.position.y + (450/2)) + 120 + (showSpriteDisplayGood.size.height/2))
        showSpriteDisplayGood.zPosition = 100
        introNode.addChild(showSpriteDisplayGood)
        
        //the icon(tick or X)
        iconGoodDisplay = SKSpriteNode(texture: getIconSprite(typeSpriteIcon: "good"))
        iconGoodDisplay.position = CGPoint(x: showSpriteDisplayGood.position.x - (showSpriteDisplayGood.size.width/2) - 100, y: showSpriteDisplayGood.position.y)
        iconGoodDisplay.zPosition = 100
        introNode.addChild(iconGoodDisplay)
        
        //the text name of the good
        textGoodShowDisplay.text = goodTextDisplay[currentNumberGoodSprite]
        textGoodShowDisplay.fontSize = 50
        textGoodShowDisplay.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        textGoodShowDisplay.zPosition = 100
        textGoodShowDisplay.position = CGPoint(x: self.size.width/2 + 270 , y: showSpriteDisplayGood.position.y - 25)
        introNode.addChild(textGoodShowDisplay)
        
        showSpriteDisplayBad = SKSpriteNode(texture: getShowSprite(typeSpriteUi: "bad", currentNumber: (currentNumberBadSprite + 1)))
        showSpriteDisplayBad.position = CGPoint(x: self.size.width/2, y:(rect2.position.y + (450/2)) - 120 + (showSpriteDisplayBad.size.height/2))
        showSpriteDisplayBad.zPosition = 100
        introNode.addChild(showSpriteDisplayBad)
        
        iconBadDisplay = SKSpriteNode(texture: getIconSprite(typeSpriteIcon: "bad"))
        iconBadDisplay.position = CGPoint(x: showSpriteDisplayBad.position.x - (showSpriteDisplayBad.size.width/2) - 100, y: showSpriteDisplayBad.position.y)
        iconBadDisplay.zPosition = 100
        introNode.addChild(iconBadDisplay)
        
        textBadShowDisplay.text = badTextDisplay[currentNumberBadSprite]
        textBadShowDisplay.fontSize = 50
        textBadShowDisplay.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        textBadShowDisplay.zPosition = 100
        textBadShowDisplay.position = CGPoint(x: self.size.width/2 + 270 , y: showSpriteDisplayBad.position.y - 25)
        introNode.addChild(textBadShowDisplay)
        
        
        introLabelTimer = SKLabelNode(fontNamed: "AllerDisplay")
        introLabelTimer.fontSize = 200
        introLabelTimer.zPosition = 130
        introLabelTimer.fontColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        introLabelTimer.position = CGPoint(x: frame.midX , y: frame.midY  )
        introLabelTimer.text = "\(introTime)"
        introNode.addChild(introLabelTimer)
        
        let bgIntro = SKSpriteNode(color: UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.50), size: CGSize(width: playAbleWidth, height: self.size.height))
        bgIntro.position = CGPoint(x: frame.midX , y: frame.midY)
        bgIntro.zPosition = 97
        introNode.addChild(bgIntro)
        
        
        //try find the bg game songs
        do {
            
            audioPlayer = try AVAudioPlayer(contentsOf: bgSound as URL)
            audioPlayer.play()
            audioPlayer.numberOfLoops = -1
            
        } catch {
            
            print("Problem in getting File")
        }
        
        getHighscore()
        
        if(highscore == 0)
        {
            isTutorialisShow_1 = 0
        }
        else
        {
            isTutorialisShow_1 = 1
        }
        
        if(isTutorialisShow_1 == 0)
        {
            currentGameState = gameState.isTutorialShow_1
            
            addChild(showFirstTimeTutorial)
            //            let tempX = self.size.width/2 - playAbleWidth/2
            
            let infoText = SKLabelNode(fontNamed: "H.H. Samuel")
            infoText.text = "Check icon is a good item meanwhile the cross\nicon is a bad item"
            infoText.fontSize = 60
            infoText.horizontalAlignmentMode = .center
            infoText.fontColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
            let testMsg = infoText.multilined(lineSpacing: 10)
            testMsg.position = CGPoint(x: self.size.width/2, y: 730)
            testMsg.zPosition = 100
            showFirstTimeTutorial.addChild(testMsg)
        }
            
            //start the timer after the sprite set up
        else
        {
            currentGameState = gameState.intro // make the game in intro state
            introTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimerIntro), userInfo: nil, repeats: true)
        }
    }
    
    func updateTimerIntro() //timer ticking for the intro
    {
        introTime -= 1
        introLabelTimer.text = "\(introTime)"
        if(introTime == 0)
        {
            //if reach 0 stop the time n straight start the game
            introTimer.invalidate()
            rstartGame()
        }
    }
    
    func rstartGame() {
        
        //remove the intro stuff
        introNode.removeAllChildren()
        introNode.removeFromParent()
        
        //change the state into game state
        currentGameState = gameState.inGame
        
        addChild(moveableParents)
        addChild(UIdisplayNode)
        addChild(UIGameOverNode)
        
        //setting up the animation for the character animation
        textureAtlas_1 = SKTextureAtlas(named: "playerAnim")
        for i in 1...textureAtlas_1.textureNames.count + 1
        {
            var textureName = ""
            if(i == textureAtlas_1.textureNames.count + 1)
            {
                textureName = "guy_01.png"
            }
            else
            {
                textureName = "guy_0\(i).png"
            }
            textureArray_1.append(SKTexture(imageNamed: textureName))
        }
        //starting the timer according for respective function
        characTimerAnim() //for character animation
        resetNormalTimer() //for normal timer tick for every sec
        resetGoodSpriteTimer() //time for good stuff to spawn
        resetBadSpriteTimer() //time for bad stuff to spawn
        
        //disable multiple touch in the game
        self.view?.isMultipleTouchEnabled = false
        
    }
    
    func updateTimerGame()
    {
        //add secTime += 1 for every sec
        secTime += 1
        gameScore += 1
        scoreLabel.text = timeString(time: TimeInterval(gameScore))
        
        //for every 5 sec, will show the good/bad indicator accordingly
        //starting showing good stuff first n then the other 5 sec will show bad stuff n repeated
        if(secTime%5 == 0 && (currentPoolGoodArray.count < 10 || currentPoolBadArray.count < 10))
        {
            //            print("original array : \(arrayNumBad) + \n current pool array : \(currentPoolBadArray)")
            if(changeBadGood == false)
            {
                let tempGood = Int(arc4random_uniform(UInt32(arrayNumGood.count)))
                currentNumberGoodSprite = arrayNumGood[tempGood]
                
                arrayNumGood = arrayNumGood.filter(){$0 != currentNumberGoodSprite}
                currentPoolGoodArray.append(currentNumberGoodSprite)
                
                durationSpawnGoodThings -= 0.1
                showNewGoodSprite = true
                changeBadGood = true
            }
            else
            {
                let tempBad = Int(arc4random_uniform(UInt32(arrayNumBad.count)))
                currentNumberBadSprite =  arrayNumBad[tempBad]
                
                //                print("currentNumberGoodSprite : \(currentNumberBadSprite)")
                
                arrayNumBad = arrayNumBad.filter(){$0 != currentNumberBadSprite}
                currentPoolBadArray.append(currentNumberBadSprite)
                
                durationSpawnBadThings -= 0.1
                showNewBadSprite = true
                changeBadGood = false
            }
            
            //show the sprite indicator at bottom
            showNewSpriteComing()
        }
        
    }
    
    func timeString(time:TimeInterval) -> String {
        //        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func resetGoodSpriteTimer()
    {
        spawnGoodSprite = Timer.scheduledTimer(timeInterval: durationSpawnGoodThings, target: self, selector: #selector(self.addGoodThings), userInfo: nil, repeats: true)
    }
    
    func resetBadSpriteTimer()
    {
        spawnBadSprite  = Timer.scheduledTimer(timeInterval: durationSpawnBadThings, target: self, selector: #selector(self.addBadThings), userInfo: nil, repeats: true)
    }
    
    func resetNormalTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimerGame), userInfo: nil, repeats: true)
    }
    
    func characTimerAnim()
    {
        characAnimTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.animationCharac), userInfo: nil, repeats: true)
    }
    
    //randomize for diff CGpoint inside the game area
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat
    {
        return random() * (max-min) + min
    }
    
    func animationCharac()
    {
        player.run(SKAction.animate(with: textureArray_1, timePerFrame: 0.3))
    }
    
    //spawning function for good and bad things
    func addGoodThings()
    {
        let tempGood = Int(arc4random_uniform(UInt32(currentPoolGoodArray.count)))
        let tempGood2 = currentPoolGoodArray[tempGood]
        
        let goodSprite = MoveableSprite(typeSprite : .goodSprite , number : tempGood2, checkingBool : true)
        goodSprite.name = "good_"
        
        sameFunc(spritePassed: goodSprite, spriteTypePassed: .goodSprite)
    }
    
    var firstBadStuff = true
    var firstBadStuffChecked = true
    var firstBadStuffSprite = SKSpriteNode()
    
    func addBadThings()
    {
        let tempBad = Int(arc4random_uniform(UInt32(currentPoolBadArray.count)))
        let tempBad2 = currentPoolBadArray[tempBad]
        let tempBad3 = tempBad2 + 10
        
        let badSprite = MoveableSprite(typeSprite: .badSprite , number : tempBad3, checkingBool : true)
        badSprite.name = "bad_"
        
        sameFunc(spritePassed: badSprite, spriteTypePassed: .badSprite)
    }
    
    func sameFunc(spritePassed : SKSpriteNode , spriteTypePassed : spriteType)
    {
        spritePassed.physicsBody = SKPhysicsBody(circleOfRadius: max(100,100)) //rectangleOf:spritePassed.size
        spritePassed.physicsBody!.isDynamic = true
        spritePassed.zPosition = 50
        
        if(spriteTypePassed == .goodSprite)
        {
            spritePassed.physicsBody!.categoryBitMask = PhysicsCategory.goodStuffPhysic
            spritePassed.physicsBody!.contactTestBitMask = PhysicsCategory.playerPhysic
            spritePassed.physicsBody!.collisionBitMask = PhysicsCategory.None
            
            countGoodStuff += 1
        }
        else
        {
            spritePassed.physicsBody!.categoryBitMask = PhysicsCategory.badSTuffPhysic
            spritePassed.physicsBody!.contactTestBitMask = PhysicsCategory.playerPhysic
            spritePassed.physicsBody!.collisionBitMask = PhysicsCategory.None
            
            countBadStuff += 1
            
            if(firstBadStuff == true && isTutorialisShow_1 == 0)
            {
                firstBadStuffSprite = spritePassed
                firstBadStuff = false
                firstBadStuffChecked = false
            }
        }
        
        let randomXaxisSpawn = random(min: spritePassed.size.width/2 , max: size.width - spritePassed.size.width/2)
        let randomYaxisSpawn = random(min: spritePassed.size.height/2 , max: size.height - spritePassed.size.height/2)
        
        let diceRoll = Int(arc4random_uniform(4))
        
        if diceRoll == 0
        {
            spritePassed.position = CGPoint(x: 0 , y: randomYaxisSpawn)
        }
        else if diceRoll == 1
        {
            spritePassed.position = CGPoint(x: randomXaxisSpawn , y: 0)
        }
        else if diceRoll == 2
        {
            spritePassed.position = CGPoint(x: randomXaxisSpawn , y: size.height)
        }
        else
        {
            spritePassed.position = CGPoint(x: size.width, y: randomYaxisSpawn)
        }
        
        moveableParents.addChild(spritePassed)
        
        //distance = speed * time
        //calculate distance formula for future usage
        let dx : CGFloat = player.position.x - spritePassed.position.x;
        let dy : CGFloat = player.position.y - spritePassed.position.y;
        
        let magnitude : CGFloat = sqrt(dx*dx+dy*dy);
        let timeTravel = random(min: CGFloat(2.0), max: CGFloat(4.0))
        let speed = magnitude/timeTravel
        
        //set the moving speed of the sprite when moving towards player
        //used for when pausing or no movement happen when touch the moveable sprite
        let testMoveableSprite = spritePassed as? MoveableSprite
        testMoveableSprite?.setSpeed(saveSpeed: speed)
        testMoveableSprite?.setTime(saveTime: timeTravel)
        
        let actionMove = SKAction.move(to: CGPoint(x: player.position.x ,y: player.position.y), duration: TimeInterval(timeTravel))
        let actionMoveDone  = SKAction.removeFromParent()
        spritePassed.run(SKAction.sequence([actionMove,actionMoveDone]))
    }
    
    var btnGameOverTouched = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first // get the first touch
        let location = touch?.location(in: self)
        self.view?.isMultipleTouchEnabled = false //disable multiple touch
        
        if(currentGameState == gameState.inGame)
        {
            //if the current touch at the location is a moveablesprite(good/bad stuff)
            if let moveSprite = atPoint(location!) as? MoveableSprite
            {
                firstTouchObj = location! //save the first location of the touch
                currentchild = moveSprite //get the one we touch n set as current good/bad stuff we holding
                moveSprite.removeAllActions() // remove the moving when we touched it
                currentchild.run(SKAction.scale(to: 1.5, duration: 0.2)) // and scaled it to bigger
                //                moveSprite.changeSpriteWhenTouch()
                //                print("began")
                
                //here is some bool for the moving/end touched
                checkTrue = true
                moveableMove = false
                checkingUpdate = true
            }
        }
            
        else if(currentGameState == gameState.pause)
        {
            let touchStuff = self.atPoint(location!)
            if(touchStuff == firstBadStuffSprite)
            {
                firstTouchObj = location!
                firstBadStuffSprite.run(SKAction.scale(to: 1.5, duration: 0.2))
                
                checkTrue = true
                moveableMove = false
                checkingUpdate = true
                
            }
            
        }
            
        else if(currentGameState == gameState.gameOver)
        {
            let touchedNode = self.atPoint(location!)
            if let name = touchedNode.name //if the touchnode got name
            {
                if name == endGameButton.name //n its the endgameButton name
                {
                    //change the button sprite and make press button true
                    endGameButton.texture = SKTexture(imageNamed: "btn_continue_02")
                    btnGameOverTouched = true
                }
            }
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view?.isMultipleTouchEnabled = false
        let touch = touches.first
        let location = touch?.location(in: self)
        
        if(currentGameState == gameState.inGame)
        {
            if(checkTrue == true)
            {
                //move the child to the moved position
                currentchild.position = location!
                
                moveableMove = true
                //                print("moving")
                
                //update the first position everytime the player move the spirte more than 3px or less than 3px
                if(checkTrue == true && moveableMove == true && checkingUpdate == true)
                {
                    if (firstTouchObj.x - (location?.x)! > 3 || firstTouchObj.x - (location?.x)! < -3  ||
                        firstTouchObj.y - (location?.y)! > 3 || firstTouchObj.y - (location?.y)! < -3)
                    {
                        firstTouchObj = location!
                        //                        print("change")
                    }
                }
            }
            
        }
            
        else if(currentGameState == gameState.pause)
        {
            if(checkTrue == true)
            {
                firstBadStuffSprite.position = location!
                moveableMove = true
                
                if(checkTrue == true && moveableMove == true && checkingUpdate == true)
                {
                    if (firstTouchObj.x - (location?.x)! > 3 || firstTouchObj.x - (location?.x)! < -3  ||
                        firstTouchObj.y - (location?.y)! > 3 || firstTouchObj.y - (location?.y)! < -3)
                    {
                        firstTouchObj = location!
                    }
                }
            }
        }
            
        else if(currentGameState == gameState.gameOver)
        {
            if(btnGameOverTouched == true)
            {
                //recheck again, if the finger moved out from the game over button, then change sprite and the game cant be continue
                let touchedNode = self.atPoint(location!)
                if let name = touchedNode.name
                {
                    if name != endGameButton.name
                    {
                        btnGameOverTouched = false
                        endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
                    }
                }
                    
                else
                {
                    btnGameOverTouched = false
                    endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
                }
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let location = touch?.location(in: self)
        
        if(currentGameState == gameState.inGame)
        {
            //if got sprite and the sprite are moving
            if checkTrue == true && moveableMove == true
            {
                //                let testMoveableSprite = currentchild as? MoveableSprite
                //                print("end")
                
                checkTrue = false
                self.view?.isMultipleTouchEnabled = true
                
                lastTouchObj = location! //get the last position of the drag obj
                currentchild.run(SKAction.scale(to: 1, duration: 0.2)) //rescale back to original size
                
                //calculate the end of position which out of screen position
                let targetDestination = gradient(firstPosition: firstTouchObj, lastPosition: lastTouchObj , screenHeight: self.size.height , screenWidth: self.size.width)
                //get the time of the sprite so, the speed will be define according to wat we have set
                let time = setSpriteTime(secondPoint: targetDestination , firstPoint: lastTouchObj, defSpeed: true , spriteRelated: currentchild)
                let actionMove2 = SKAction.move(to : targetDestination , duration : TimeInterval(time))
                let actionMoveDone2 = SKAction.removeFromParent()
                
                currentchild.removeAllActions()
                
                //                 testMoveableSprite?.changeSpriteWhenTouch()
                currentchild.run(SKAction.sequence([actionMove2,actionMoveDone2]))
                run(SKAction.playSoundFileNamed("throw.wav", waitForCompletion: false))
            }
                
                //            if got sprite n the sprite not moving, cont back to origin
            else if (moveableMove == false && checkTrue == true)
            {
                checkTrue = false
                self.view?.isMultipleTouchEnabled = true
                
                //                let testMoveableSprite = currentchild as? MoveableSprite
                
                let time = setSpriteTime(secondPoint: player.position, firstPoint: lastTouchObj, defSpeed: false, spriteRelated: currentchild)
                
                //                print("speed end : \(time)")
                let rescaleBack = SKAction.scale(to: 1, duration: 0.2)
                let actionMove = SKAction.move(to: CGPoint(x: player.position.x ,y: player.position.y), duration: TimeInterval(time))
                let actionMoveDone  = SKAction.removeFromParent()
                
                currentchild.removeAllActions()
                currentchild.run(rescaleBack)
                //                testMoveableSprite?.changeSpriteWhenTouch()
                
                currentchild.run(SKAction.sequence([actionMove,actionMoveDone]))
                //                currentchild.run(SKAction.scale(to: 1, duration: 0.2))
                //                print("no move")
            }
        }
            
        else if(currentGameState == gameState.isTutorialShow_1)
        {
            for child in showFirstTimeTutorial.children
            {
                child.removeAllActions()
                child.removeFromParent()
            }
            
            currentGameState = gameState.intro // make the game in intro state
            introTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimerIntro), userInfo: nil, repeats: true)
        }
            
        else if(currentGameState == gameState.pause)
        {
            firstBadStuffSprite.zPosition = 13
            //if got sprite and the sprite are moving
            if checkTrue == true && moveableMove == true
            {
                checkTrue = false
                self.view?.isMultipleTouchEnabled = true
                
                lastTouchObj = location! //get the last position of the drag obj
                
                //calculate the end of position which out of screen position
                let targetDestination = gradient(firstPosition: firstTouchObj, lastPosition: lastTouchObj , screenHeight: self.size.height , screenWidth: self.size.width)
                
                //get the time of the sprite so, the speed will be define according to wat we have set
                let time = setSpriteTime(secondPoint: targetDestination , firstPoint: lastTouchObj, defSpeed: true , spriteRelated: firstBadStuffSprite)
                let actionMove2 = SKAction.move(to : targetDestination , duration : TimeInterval(time))
                let actionMoveDone2 = SKAction.removeFromParent()
                
                firstBadStuffSprite.removeAllActions()
                firstBadStuffSprite.run(SKAction.scale(to: 1, duration: 0.2)) //rescale back to original size
                firstBadStuffSprite.run(SKAction.sequence([actionMove2,actionMoveDone2]))
                
                for child in moveableParents.children
                {
                    //make the children go back towards the player
                    if let spriteNode = child as? MoveableSprite
                    {
                        if(firstBadStuffSprite != spriteNode)
                        {
                            let time = setSpriteTime(secondPoint: player.position, firstPoint: spriteNode.position, defSpeed: false, spriteRelated: spriteNode)
                            
                            let actionMove = SKAction.move(to: CGPoint(x: player.position.x ,y: player.position.y), duration: TimeInterval(time))
                            let actionMoveDone  = SKAction.removeFromParent()
                            
                            spriteNode.removeAllActions()
                            spriteNode.run(SKAction.sequence([actionMove,actionMoveDone]))
                            spriteNode.run(SKAction.scale(to: 1, duration: 0.2))
                        }
                    }
                }
            }
                
            else
            {
                for child in moveableParents.children
                {
                    //make the children go back towards the player
                    if let spriteNode = child as? MoveableSprite
                    {
                        //                        if(firstBadStuffSprite != spriteNode)
                        //                        {
                        let time = setSpriteTime(secondPoint: player.position, firstPoint: spriteNode.position, defSpeed: false, spriteRelated: spriteNode)
                        
                        let actionMove = SKAction.move(to: CGPoint(x: player.position.x ,y: player.position.y), duration: TimeInterval(time))
                        let actionMoveDone  = SKAction.removeFromParent()
                        
                        spriteNode.removeAllActions()
                        spriteNode.run(SKAction.sequence([actionMove,actionMoveDone]))
                        spriteNode.run(SKAction.scale(to: 1, duration: 0.2))
                        //                        }
                    }
                }
                
                
            }
            
            for child in showBadFirstTimeNode.children
            {
                child.removeAllActions()
                child.removeFromParent()
            }
            
            showBadFirstTimeNode.removeFromParent()
            
//            userDefults.set(1, forKey: "tutorial_1_done")
//            userDefults.synchronize()
//            isTutorialisShow_1 = userDefults.object(forKey: "tutorial_1_done") as! Int
            
            resetNormalTimer()
            resetGoodSpriteTimer()
            resetBadSpriteTimer()
            currentGameState = gameState.inGame
            
        }
            
        else if(currentGameState == gameState.gameOver)
        {
            //if the player finger never goes out from cont button, cont restart game
            if(btnGameOverTouched == true)
            {
                sendHighscore()
                endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
                audioPlayer.stop()
                restartGameLvl()
            }
        }
    }
    
    //get the time of the sprite so, the speed will be define according to wat we have set
    func setSpriteTime(secondPoint : CGPoint, firstPoint : CGPoint, defSpeed : Bool , spriteRelated : SKSpriteNode) -> CGFloat
    {
        var speedA : CGFloat = 0
        
        if (defSpeed == true)
        {
            speedA = 4000
        }
        else
        {
            let testMoveableSprite = spriteRelated as? MoveableSprite
            speedA = (testMoveableSprite?.getSpeed())!
        }
        
        //distance = speed * time
        let dx : CGFloat = secondPoint.x - firstPoint.x;
        let dy : CGFloat = secondPoint.y - firstPoint.y;
        
        let magnitude : CGFloat = sqrt(dx*dx+dy*dy);
        
        let timeCalculate = magnitude/speedA
        
        return timeCalculate;
    }
    
    //when collider meets another collider
    func didBegin(_ contact: SKPhysicsContact)
    {
        var firstBody : SKPhysicsBody
        var secondBody : SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask
        {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else
        {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.categoryBitMask == PhysicsCategory.playerPhysic && secondBody.categoryBitMask == PhysicsCategory.badSTuffPhysic
        {
            let tempBadSprite = secondBody.node as? SKSpriteNode
            badHitPlayer(badSpriteGotHit: tempBadSprite!)
        }
        
        if firstBody.categoryBitMask == PhysicsCategory.playerPhysic && secondBody.categoryBitMask == PhysicsCategory.goodStuffPhysic
        {
            let goodStuff = secondBody.node as? SKSpriteNode
            run(SKAction.playSoundFileNamed("correct.wav", waitForCompletion: false))
            
            gameScore += 1
            scoreLabel.text = timeString(time: TimeInterval(gameScore))
            
            let timePlus = SKLabelNode(fontNamed: "AllerDisplay", andText: "+1", andSize: 120, withShadow: UIColor.black)
            timePlus?.zPosition = 1000
            
            timePlus?.position = CGPoint(x: (player.position.x), y: (player.position.y + (player.size.height/2)) )
            addChild(timePlus!)
            
            goodStuff?.removeFromParent()
            let moveUp = SKAction.moveBy(x: 0, y: +50, duration: 0.5)
            let removeActionText = SKAction.removeFromParent()
            timePlus?.run(SKAction.sequence([moveUp,removeActionText]))
            
        }
    }
    
    //when bad stuff hit player collider, game over
    func badHitPlayer(badSpriteGotHit : SKSpriteNode)
    {
        print("dead")
        badSpriteGotHit.removeFromParent()
        player.removeFromParent()
        
        //stop all the spawn/timer
        characAnimTimer.invalidate()
        timer.invalidate()
        spawnGoodSprite.invalidate()
        spawnBadSprite.invalidate()
        
        //make all stuff flying stop moving
        for child in moveableParents.children
        {
            child.removeAllActions()
        }
        
        showBadFirstTimeNode.removeAllActions()
        showBadFirstTimeNode.removeAllChildren()
        showBadFirstTimeNode.removeFromParent()
        
        currentGameState = gameState.gameOver
        
        let anim_fromTop : CGFloat = 50
        
        //make the game over ui spawn/shown
        let shapeSizeX: CGFloat = 1000
        let shapeSizeY: CGFloat = 1200
        let shape = SKShapeNode()
        shape.path = UIBezierPath(roundedRect: CGRect(x: -(shapeSizeX / 2), y: -(shapeSizeY / 2), width: shapeSizeX, height: shapeSizeY), cornerRadius: 30).cgPath
        shape.position = CGPoint(x: frame.midX , y: frame.midY + 80 + anim_fromTop)
        shape.fillColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.strokeColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.lineWidth = 10
        shape.zPosition = 80
        UIGameOverNode.addChild(shape)
        
        let shape2SizeX: CGFloat = 950
        let shape2SizeY: CGFloat = 1150
        let shape2 = SKShapeNode()
        shape2.path = UIBezierPath(roundedRect: CGRect(x: -(shape2SizeX / 2), y: -(shape2SizeY / 2), width: shape2SizeX , height: shape2SizeY), cornerRadius: 20).cgPath
        shape2.position = CGPoint(x: frame.midX , y: frame.midY + 80 + anim_fromTop)
        shape2.fillColor = UIColor.white
        shape2.strokeColor = UIColor.white
        shape2.lineWidth = 10
        shape2.zPosition = 81
        UIGameOverNode.addChild(shape2)
        
        let temp_msg = SKLabelNode(fontNamed: "H.H. Samuel")
        temp_msg.text = "you've violated the gift policy."
        temp_msg.fontSize = 80
        temp_msg.horizontalAlignmentMode = .center
        temp_msg.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        
        let text_top = temp_msg.multilined(lineSpacing: CGFloat(10))
        text_top.position = CGPoint(x: frame.midX, y: frame.midY + 490 + anim_fromTop) // - (text_msg.frame.height/2)
        text_top.zPosition = 100
        UIGameOverNode.addChild(text_top)
        
        let highestPointScoreLabel = SKLabelNode(fontNamed: "AllerDisplay")
        highestPointScoreLabel.text = "Highscore : \(highscore)"
        highestPointScoreLabel.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        highestPointScoreLabel.fontSize = 80
        highestPointScoreLabel.position = CGPoint(x: frame.midX, y: frame.midY + 180 + anim_fromTop)
        highestPointScoreLabel.zPosition = 100
        UIGameOverNode.addChild(highestPointScoreLabel)
        
        let currenttPointScoreLabel = SKLabelNode(fontNamed: "AllerDisplay")
        currenttPointScoreLabel.text = "Current : \(gameScore)"
        currenttPointScoreLabel.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        currenttPointScoreLabel.fontSize = 80
        currenttPointScoreLabel.position = CGPoint(x: frame.midX, y: frame.midY + 80 + anim_fromTop)
        currenttPointScoreLabel.zPosition = 100
        UIGameOverNode.addChild(currenttPointScoreLabel)
        
        let workerComputer = SKSpriteNode(imageNamed: "gameover_icon")
        workerComputer.setScale(1)
        workerComputer.position = CGPoint(x: frame.midX, y: frame.midY - 250 + anim_fromTop)
        workerComputer.zPosition = 101
        UIGameOverNode.addChild(workerComputer)
        
        let workerComputer1 = SKSpriteNode(imageNamed: "gameover_text")
        workerComputer1.setScale(1)
        workerComputer1.position = CGPoint(x: frame.midX, y: frame.midY + 680 + anim_fromTop)
        workerComputer1.zPosition = 101
        UIGameOverNode.addChild(workerComputer1)
        
        endGameButton = SKSpriteNode(imageNamed: "btn_continue_01")
        endGameButton.setScale(1)
        endGameButton.position = CGPoint(x: frame.midX, y: frame.midY - 670 + anim_fromTop)
        endGameButton.name = "endGameButton"
        endGameButton.zPosition = 101
        UIGameOverNode.addChild(endGameButton)
        
        let moveDown = SKAction.moveBy(x: 0, y: -50, duration: 0.2)
        UIGameOverNode.run(moveDown)
        
        let bgIntro = SKSpriteNode(color: UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.50), size: CGSize(width: playAbleWidth, height: self.size.height))
        bgIntro.position = CGPoint(x: frame.midX , y: frame.midY)
        bgIntro.zPosition = 70
        addChild(bgIntro)
    }
    
    func SDistanceBetweenPoints(first : CGPoint, second : CGPoint) -> CGFloat  {
        return CGFloat(hypotf(Float(second.x - first.x), Float(second.y - first.y)));
    }
    
    override func update(_ currentTime: TimeInterval)
    {
        if(firstBadStuff == false && firstBadStuffChecked == false && currentGameState == gameState.inGame)
        {
            if(SDistanceBetweenPoints(first: firstBadStuffSprite.position, second: player.position) < 450 )
            {
                firstBadStuffChecked = true
                addChild(showBadFirstTimeNode)
                
                timer.invalidate()
                spawnGoodSprite.invalidate()
                spawnBadSprite.invalidate()
                
                for child in moveableParents.children
                {
                    child.removeAllActions()
                }
                
                currentGameState = gameState.pause
                
                showBadFirstTimeNode.addChild(fingerShow)
                var tempTarget : CGPoint = CGPoint(x: 0, y: 0)
                var tempPosition : CGPoint = CGPoint(x: 0, y: 0)
                
                fingerShow.zPosition = 200
                fingerShow.position.y = firstBadStuffSprite.position.y - firstBadStuffSprite.size.height/2
                
                if(firstBadStuffSprite.position.x < self.playAbleWidth/2)
                {
                    fingerShow.position.x = firstBadStuffSprite.position.x + firstBadStuffSprite.size.width/2
                    tempPosition = fingerShow.position
                    tempTarget = CGPoint(x: -200, y: 0)
                }
                else
                {
                    fingerShow.position.x = firstBadStuffSprite.position.x + firstBadStuffSprite.size.width/2
                    tempPosition = fingerShow.position
                    tempTarget = CGPoint(x: 200, y: 0)
                }
                
                
                
                let spriteDown = SKAction.run {
                    self.fingerShow.texture = SKTexture(imageNamed: "finger_down")
                }
                let goToOriginPosition = SKAction.run {
                    self.fingerShow.position = CGPoint(x: tempPosition.x, y: tempPosition.y) //self.firstBadStuffSprite.position.y
                }
                
                let positionTemp = CGPoint(x: fingerShow.position.x + (tempTarget.x), y: fingerShow.position.y)
                let actionMoveFingerTo = SKAction.move(to: positionTemp, duration: 1)
                
                let spriteUp = SKAction.run {
                    self.fingerShow.texture = SKTexture(imageNamed: "finger_up")
                }
                let waitforWhile = SKAction.wait(forDuration: 0.5)
                
                fingerShow.run(SKAction.repeatForever(SKAction.sequence([spriteDown,goToOriginPosition, actionMoveFingerTo,spriteUp,waitforWhile])))
                
                let rect1 = SKShapeNode(rect: CGRect(x:self.size.width/2 - ((playAbleWidth - 100)/2) , y: 50 , width: playAbleWidth - 100, height: 200),cornerRadius: 20)
                rect1.fillColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
                rect1.strokeColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
                rect1.zPosition = 98
                showBadFirstTimeNode.addChild(rect1)
                
                let rect2 = SKShapeNode(rect: CGRect(x:self.size.width/2 - ((playAbleWidth - 150)/2) , y: 80, width: playAbleWidth - 150, height: 150),cornerRadius: 20)
                rect2.fillColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
                rect2.strokeColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
                rect2.zPosition = 99
                showBadFirstTimeNode.addChild(rect2)
                
                let textShowDisplay = SKLabelNode(fontNamed: "H.H. Samuel")
                textShowDisplay.text = "Throw away the unacceptable things according to finger"
                textShowDisplay.fontSize = 50
                textShowDisplay.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
                textShowDisplay.horizontalAlignmentMode = .center
                textShowDisplay.zPosition = 100
                textShowDisplay.position = CGPoint(x: frame.midX , y: rect2.position.y + (60 + (150/2)))
                showBadFirstTimeNode.addChild(textShowDisplay)
                
                let bgIntro = SKSpriteNode(color: UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.50), size: CGSize(width: playAbleWidth, height: self.size.height))
                bgIntro.position = CGPoint(x: frame.midX , y: frame.midY)
                bgIntro.zPosition = 90
                showBadFirstTimeNode.addChild(bgIntro)
                
                firstBadStuffSprite.zPosition = 92
            }
            
        }
    }
    
    //the drawing stuff for the good/bad show indicator
    func showNewSpriteComing()
    {
        let rect1 = SKShapeNode(rect: CGRect(x:self.size.width/2 - ((playAbleWidth - 100)/2) , y: 50 , width: playAbleWidth - 100, height: 300),cornerRadius: 20)
        rect1.fillColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
        rect1.strokeColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
        rect1.zPosition = 30
        UIdisplayNode.addChild(rect1)
        
        let rect2 = SKShapeNode(rect: CGRect(x:self.size.width/2 - ((playAbleWidth - 150)/2) , y: 80, width: playAbleWidth - 150, height: 250),cornerRadius: 20)
        rect2.fillColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        rect2.strokeColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        rect2.zPosition = 31
        UIdisplayNode.addChild(rect2)
        
        var showSpriteDisplay = SKSpriteNode()
        let textShowDisplay = SKLabelNode(fontNamed: "H.H. Samuel")
        var iconGoodBadDisplay = SKSpriteNode()
        
        if(showNewGoodSprite == true)
        {
            showSpriteDisplay = SKSpriteNode(texture: getShowSprite(typeSpriteUi: "good", currentNumber: (currentNumberGoodSprite + 1)))
            iconGoodBadDisplay = SKSpriteNode(texture: getIconSprite(typeSpriteIcon: "good"))
            textShowDisplay.text = goodTextDisplay[currentNumberGoodSprite]
            
            showNewGoodSprite = false
        }
        
        if(showNewBadSprite == true)
        {
            showSpriteDisplay = SKSpriteNode(texture: getShowSprite(typeSpriteUi: "bad", currentNumber: (currentNumberBadSprite + 1)))
            iconGoodBadDisplay = SKSpriteNode(texture: getIconSprite(typeSpriteIcon: "bad"))
            textShowDisplay.text = badTextDisplay[currentNumberBadSprite]
            
            showNewBadSprite = false
        }
        
        showSpriteDisplay.position = CGPoint(x: self.size.width/2, y: rect2.position.y + 120 + (showSpriteDisplay.size.height/2))
        showSpriteDisplay.zPosition = 32
        UIdisplayNode.addChild(showSpriteDisplay)
        
        iconGoodBadDisplay.position = CGPoint(x: showSpriteDisplay.position.x - (showSpriteDisplay.size.width/2) - 100, y: showSpriteDisplay.position.y)
        iconGoodBadDisplay.zPosition = 32
        UIdisplayNode.addChild(iconGoodBadDisplay)
        
        textShowDisplay.fontSize = 50
        textShowDisplay.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        textShowDisplay.zPosition = 32
        textShowDisplay.position = CGPoint(x: self.size.width/2 + 270 , y: showSpriteDisplay.position.y - 25)
        UIdisplayNode.addChild(textShowDisplay)
        
        UIDisplayDestroy()
    }
    
    func UIDisplayDestroy()
    {
        destroyUIDisplay  = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.removeChildUIDisplay), userInfo: nil, repeats: false)
    }
    
    func removeChildUIDisplay()
    {
        for child in UIdisplayNode.children
        {
            child.removeFromParent()
        }
        destroyUIDisplay.invalidate()
    }
    
    func getShowSprite(typeSpriteUi : String, currentNumber : Int) -> SKTexture
    {
        var temp = SKTexture()
        if(typeSpriteUi == "good")
        {
            temp = SKTexture(imageNamed: "show_accept_\(currentNumber)")
        }
        else
        {
            temp = SKTexture(imageNamed: "show_decline_\(currentNumber)")
        }
        return temp
    }
    
    func getIconSprite(typeSpriteIcon : String) -> SKTexture
    {
        var temp = SKTexture()
        if(typeSpriteIcon == "good")
        {
            temp = SKTexture(imageNamed: "show_accept")
        }
        else
        {
            temp = SKTexture(imageNamed: "show_decline")
        }
        return temp
    }
    
    func getHighscore() {
        // get highscore from server
        //        highScore = "get highscore api"
    }
    
    func sendHighscore() {
        // send highscore to server
        highscore = gameScore
    }
    
    func goToMainMenu()
    {
        let scene = GameTwo(size: self.size) // Change from GameScene -> MainMenuScene class if wan go to Main Menu
        scene.scaleMode = self.scaleMode
        let animation = SKTransition.crossFade(withDuration: 0.5) // Choose any transition you like
        self.view?.presentScene(scene, transition: animation)
    }
    
    func restartGameLvl()
    {
        //code to restart the game
        let scene = GameTwo(size: self.size) // Whichever scene you want to restart (and are in)
        scene.scaleMode = self.scaleMode
        let animation = SKTransition.crossFade(withDuration: 0.5) // ...Add transition if you like
        self.view?.presentScene(scene, transition: animation)
    }
}
